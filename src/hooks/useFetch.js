import { onMounted, ref } from 'vue'

export function useFetch (url) {
  const data = ref([])

  onMounted(async () => {
    try {
      const res = await fetch(url)
      data.value = await res.json()
    } catch (e) {
      console.log(e);
    }
  })

  return {data}
}

