import { computed, ref } from "vue";

export function useContador () {
  const contador = ref(0);

  const aumentar = () => {
    contador.value++;
  }
  const disminuir = () => {
    contador.value--;
  }

  const color = computed(() => {
    if (contador.value < 0) {
      return 'red'
    }
    else if (contador.value > 0) {
      return 'grey'
    }
    else {
      return ''
    }
  })

  return { contador, aumentar, disminuir, color }
}
